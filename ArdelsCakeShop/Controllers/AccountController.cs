﻿using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ArdelsCakeShop.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
                return View(loginViewModel);

            var user = await _userManager.FindByNameAsync(loginViewModel.Username);
            

            if (user != null)
            {
                var result = await _signInManager.PasswordSignInAsync(user, loginViewModel.Password, false, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Username and password doesn't match");
            }
            else
            {
                ModelState.AddModelError("", "User cannot be found");
            }
            
            return View(loginViewModel);
        }

        [HttpGet]
        [AllowAnonymous]        
        public IActionResult FacebookLogin()
        {

            var redirectUri = Url.Action("HandleExternalLogin", "Account");

            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Facebook", redirectUri);
            return Challenge(properties, "Facebook");
        }

        [AllowAnonymous]        
        public async Task<IActionResult> HandleExternalLogin()
        {
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, false);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }


            //get user details
            var email = info.Principal.Claims.Where(c => c.Type == ClaimTypes.Email)
                   .Select(c => c.Value).SingleOrDefault();
            var userName = email;
            var address = info.Principal.Claims.Where(c => c.Type == ClaimTypes.StreetAddress)
                .Select(c => c.Value).SingleOrDefault();
            var phoneNr = info.Principal.Claims.Where(c => c.Type == ClaimTypes.MobilePhone)
                              .Select(c => c.Value).SingleOrDefault() ?? 
                          info.Principal.Claims.Where(c => c.Type == ClaimTypes.HomePhone)
                              .Select(c => c.Value).SingleOrDefault();

            var user = await _userManager.FindByNameAsync(userName);

            if (user != null)
            {
                ModelState.AddModelError("", "Username already used, choose another!");
                var loginView = new LoginViewModel {Username = userName};
                return View("Login", loginView);
            }

            user = new ApplicationUser()
            {
                UserName = userName,
                Email = email,
                Address = address,
                PhoneNumber = phoneNr,       
            };

            var identityResult = await _userManager.CreateAsync(user);
            if (!identityResult.Succeeded)
            {
                return Forbid();
            }
            identityResult = await _userManager.AddLoginAsync(user, info);
            if (!identityResult.Succeeded)
            {
                return Forbid();
            }
            await _signInManager.SignInAsync(user, false);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(registerViewModel);
            }

            var user = await _userManager.FindByNameAsync(registerViewModel.Username);
            if (user != null)
            {
                ModelState.AddModelError("", "Username already used, choose another!");
                return View(registerViewModel);
            }

            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                ModelState.AddModelError("", "Passwords don't match!");
                return View(registerViewModel);
            }

            user = new ApplicationUser()
            {
                UserName = registerViewModel.Username,
                Email = registerViewModel.Email,
                Address = registerViewModel.Address,
                PhoneNumber = registerViewModel.PhoneNumber                
            };
            var result = await _userManager.CreateAsync(user, registerViewModel.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                foreach (var err in result.Errors)
                    ModelState.AddModelError("", err.Description);
                return View(registerViewModel);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Profile()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }

            var model = new EditProfileViewModel
            {
                PhoneNumber = user.PhoneNumber,
                Address = user.Address,
                Email = user.Email,
                Username = user.UserName
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Profile(EditProfileViewModel editViewModel)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "This user info doesn't seem legit.");

                editViewModel.Email = user.Email;
                editViewModel.Username = user.UserName;

                return View(editViewModel);
            }

            user.Address = editViewModel.Address;
            user.PhoneNumber = editViewModel.PhoneNumber;            

            var result = await _userManager.UpdateAsync(user);
            
            if (result.Succeeded)
            {
                //set the principal to the current context                

                var claims = await _signInManager.CreateUserPrincipalAsync(user);
                Thread.CurrentPrincipal = HttpContext.User = claims;                

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("", "Action cannot be performed successfully.");

            editViewModel.Email = user.Email;
            editViewModel.Username = user.UserName;

            return View(editViewModel);
        }


        [HttpGet]        
        [Authorize]
        public IActionResult Logout()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Logout")]
        [Authorize]
        public async Task<IActionResult> LogoutPost()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
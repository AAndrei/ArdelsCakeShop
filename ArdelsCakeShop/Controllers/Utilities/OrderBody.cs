﻿using System.Text;
using ArdelsCakeShop.Models.Orders;
using Microsoft.AspNetCore.Http;

namespace ArdelsCakeShop.Controllers.Utilities
{
    public static class OrderBody
    {
        public static string OrderReceived(Order order)
        {
            StringBuilder builder = new StringBuilder();            
            builder.Append("Order id: " + order.OrderId);
            builder.Append("<br/>From: " + order.FirstName + " " + order.LastName);
            builder.Append("<br/>Address: " + order.AddressLine);
            builder.Append("<br/>Phone number: " + order.PhoneNumber);
            builder.Append("<br/>Placed at: " + order.OrderPlaced);
            builder.Append("<br/>Order details: ");
            builder.Append("<table>");
            builder.Append("<tr><th>Cake</th><th>Amount</th><th>Price/unit</th></tr>");
            foreach (var od in order.OrderLines)
            {
                builder.Append("<tr><td>" + od.Cake.Name + "</td><td>" + od.Amount + "</td><td>" + od.Price + "</td></tr>");
            }
            builder.Append("</table>");
            builder.Append("<br/>Total: " + order.OrderTotal);
            
            return builder.ToString();
        }

        public static string OrderPlaced(Order order, string link)
        {
            
            StringBuilder builder = new StringBuilder();
            builder.Append("Your order has been placed!");            
            builder.Append("<br/>Order details: ");
            builder.Append("<br/>From: " + order.FirstName + " " + order.LastName);
            builder.Append("<br/>Address: " + order.AddressLine);
            builder.Append("<br/>Phone number: " + order.PhoneNumber);
            builder.Append("<br/>Placed at: " + order.OrderPlaced);
            builder.Append("<table>");
            builder.Append("<tr><th>Cake</th><th>Amount</th><th>Price/unit</th></tr>");
            foreach (var od in order.OrderLines)
            {
                builder.Append("<tr><td>" + od.Cake.Name + "</td><td>" + od.Amount + "</td><td>" + od.Price + "</td></tr>");
            }
            builder.Append("</table>");
            builder.Append("<br/>Total: " + order.OrderTotal);

            builder.Append("<br/><br/>Click the <a href= "+ '"' + link + '"' + ">link</a> to confirm your order!");
            return builder.ToString();
        }

        public static string OrderStatus(Order order)
        {
            StringBuilder builder = new StringBuilder();            
            builder.Append("<br/>Order details: ");
            builder.Append("<br/>From: " + order.FirstName + " " + order.LastName);
            builder.Append("<br/>Address: " + order.AddressLine);
            builder.Append("<br/>Phone number: " + order.PhoneNumber);
            builder.Append("<br/>Placed at: " + order.OrderPlaced);
            builder.Append("<table>");
            builder.Append("<tr><th>Cake</th><th>Amount</th><th>Price/unit</th></tr>");
            foreach (var od in order.OrderLines)
            {
                builder.Append("<tr><td>" + od.Cake.Name + "</td><td>" + od.Amount + "</td><td>" + od.Price + "</td></tr>");
            }
            builder.Append("</table>");
            builder.Append("<br/>Total: " + order.OrderTotal);
            
            return builder.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.Models.Orders;
using Microsoft.AspNetCore.Identity;

namespace ArdelsCakeShop.Controllers.Utilities
{
    public class EmailSender
    {
        
        private readonly SmtpClient _smtpClient;
        private readonly MailAddress _myAddress;

        private MailAddress _clientAddress;
        
        private string _subject;
        private string _body;

        private static EmailSender _instance;

        public static EmailSender Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EmailSender();
                }
                return _instance;
            }
        }
        private EmailSender()
        {         
            var fromAddress = Startup.HiddenKeys["Username"];
            var fromPassword = Startup.HiddenKeys["Password"];

            _myAddress = new MailAddress(fromAddress);

            _smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_myAddress.Address, fromPassword)
            };
        }



        public void Send(string toAddress, string subject, string body, bool isBodyHtml = false)
        {
            _clientAddress = new MailAddress(toAddress);
            _subject = subject;
            _body = body;

            MailMessage mailMessage = new MailMessage();
          
            mailMessage.From = _myAddress;
            mailMessage.To.Add(_clientAddress);

            mailMessage.IsBodyHtml = isBodyHtml;
            mailMessage.Body = _body;
            mailMessage.Subject = _subject;

            try
            {
                _smtpClient.Send(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }



        public void Receive(string fromAddress, string subject, string body, bool isBodyHtml = false)
        {
            _clientAddress = _myAddress;
            _subject = subject;
            _body = body;


            MailMessage mailMessage = new MailMessage();

            mailMessage.From = _clientAddress;
            mailMessage.To.Add(_myAddress);

            mailMessage.IsBodyHtml = isBodyHtml;
            mailMessage.Body = "You received an email from: " + fromAddress + "\n\n" + _body;
            mailMessage.Subject = _subject;
            try
            {
                _smtpClient.Send(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void NotifyAdminsNewOrder(Order order, List<ApplicationUser> admins)
        {
            foreach (var user in admins)
            {
                Send(user.Email, "New order received", OrderBody.OrderReceived(order), true);
            }
        }

        public void NotifyAdminsConfirmedOrder(Order order, List<ApplicationUser> admins)
        {
            foreach (var user in admins)
            {
                Send(user.Email, "Confirmed order", OrderBody.OrderReceived(order), true);
            }
        }

        public void NotifyAdminsRejectedOrder(Order order, List<ApplicationUser> admins)
        {
            foreach (var user in admins)
            {
                Send(user.Email, "Rejected order", OrderBody.OrderReceived(order), true);
            }
        }

        public void NotifyAdminsCompletedOrder(Order order, List<ApplicationUser> admins)
        {
            foreach (var user in admins)
            {
                Send(user.Email, "Completed order", OrderBody.OrderReceived(order), true);
            }
        }



        public void NotifyClientNewOrder(Order order, ApplicationUser client, string link)
        {
            Send(client.Email, "New order placed", OrderBody.OrderPlaced(order, link), true);
        }
        public void NotifyClientRejectedOrder(Order order, ApplicationUser client)
        {
            Send(client.Email, "Rejected order", OrderBody.OrderStatus(order), true);
        }
        public void NotifyClientCompletedOrder(Order order, ApplicationUser client)
        {
            Send(client.Email, "Completed order", OrderBody.OrderStatus(order), true);
        }
    }
}

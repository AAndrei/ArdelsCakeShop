﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ArdelsCakeShop.Controllers.Utilities;
using ArdelsCakeShop.Models.Cakes;
using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.Models.Orders;
using ArdelsCakeShop.Models.Repository;
using ArdelsCakeShop.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace ArdelsCakeShop.Controllers
{

    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IOrderRepository _orderRepository;
        private readonly ICakeRepository _cakeRepository;
        private readonly IOrdersArchiveRepository _ordersArchiveRepository;        

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            IOrderRepository orderRepository, ICakeRepository cakeRepository, IOrdersArchiveRepository ordersArchiveRepository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _orderRepository = orderRepository;
            _cakeRepository = cakeRepository;
            _ordersArchiveRepository = ordersArchiveRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult UserManagement()
        {
            var users = _userManager.Users;

            return View(users);
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                return RedirectToAction("UserManagement", _userManager.Users);
            }

            var editUser = new EditUserViewModel
            {
                Email = user.Email,
                UserName = user.UserName,
                Id = user.Id
            };           

            return View(editUser);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel userViewModel)
        {
            var user = await _userManager.FindByIdAsync(userViewModel.Id);

            if (user != null)
            {
                user.Email = userViewModel.Email;
                user.UserName = userViewModel.UserName;

                var editUser = new EditUserViewModel
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    Id = user.Id
                };

                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                    return RedirectToAction("UserManagement", _userManager.Users);

                ModelState.AddModelError("", "User not updated, something went wrong.");

                return View(editUser);
            }

            return RedirectToAction("UserManagement", _userManager.Users);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return RedirectToAction("UserManagement");
                else
                    ModelState.AddModelError("", "Something went wrong while deleting this user.");
            }
            else
            {
                ModelState.AddModelError("", "This user can't be found");
            }
            return View("UserManagement", _userManager.Users);
        }

        public IActionResult RoleManagement()
        {
            var roles = _roleManager.Roles;
            return View(roles);
        }

        public IActionResult AddRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddRole(AddRoleViewModel addRoleViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(addRoleViewModel);
            }

            var role = new IdentityRole
            {
                Name = addRoleViewModel.RoleName
            };

            var result = await _roleManager.CreateAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(addRoleViewModel);
        }

        public IActionResult BackToRoleManagement()
        {
            return RedirectToAction("RoleManagement", _roleManager.Roles);
        }

        public async Task<IActionResult> EditRole(String id)
        {
            var role = await _roleManager.FindByIdAsync(id);

            if (role == null)
            {
                ModelState.AddModelError("", "This role can't be found or edited.");
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }
                

            var editRoleViewModel = new EditRoleViewModel
            {
                Id = role.Id,
                RoleName = role.Name,
                Users = new List<string>()
            };


            foreach (var user in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                    editRoleViewModel.Users.Add(user.UserName);
            }

            return View(editRoleViewModel);

        }

        [HttpPost]
        public async Task<IActionResult> EditRole(EditRoleViewModel editRoleViewModel)
        {
            var role = await _roleManager.FindByIdAsync(editRoleViewModel.Id);

            if (role != null && !role.Name.Equals("Administrator"))
            {
                role.Name = editRoleViewModel.RoleName;

                var result = await _roleManager.UpdateAsync(role);

                if (result.Succeeded)
                    return RedirectToAction("RoleManagement", _roleManager.Roles);

                ModelState.AddModelError("", "We're sorry, but role cannot be updated");

                return View(editRoleViewModel);
            }

            ModelState.AddModelError("", "This role can't be found or edited.");
            return RedirectToAction("RoleManagement", _roleManager.Roles);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteRole(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null && !role.Name.Equals("Administrator"))
            {
                var result = await _roleManager.DeleteAsync(role);
                if (result.Succeeded)
                    return RedirectToAction("RoleManagement", _roleManager.Roles);
                ModelState.AddModelError("", "Something went wrong while deleting this role.");
            }
            else
            {
                ModelState.AddModelError("", "This role can't be found or deleted.");
            }
            return View("RoleManagement", _roleManager.Roles);
        }


        public async Task<IActionResult> AddUserToRole(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
                return RedirectToAction("RoleManagement", _roleManager.Roles);

            var addUserToRoleViewModel = new UserToRoleViewModel
            {
                RoleId = role.Id,
                Users = new List<ApplicationUser>()
            };

            foreach (var user in _userManager.Users)
            {
                if (!await _userManager.IsInRoleAsync(user, role.Name))
                {
                    addUserToRoleViewModel.Users.Add(user);
                }
            }

            return View(addUserToRoleViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddUserToRole(UserToRoleViewModel userRoleViewModel)
        {
            if (userRoleViewModel.RoleId == null)
            {
                ModelState.AddModelError("", "Role not specified.");
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }

            var role = await _roleManager.FindByIdAsync(userRoleViewModel.RoleId);

            if (role == null)
            {
                ModelState.AddModelError("", "Users or role not specified.");
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }

            else if (userRoleViewModel.UserId == null)
            {
                ModelState.AddModelError("", "User not specified.");
                return RedirectToAction("AddUserToRole", userRoleViewModel.RoleId);
            }
            
            var user = await _userManager.FindByIdAsync(userRoleViewModel.UserId);

            if (user == null)
            {
                ModelState.AddModelError("", "User not specified.");
                return RedirectToAction("AddUserToRole", userRoleViewModel.RoleId);
            }            

            var result = await _userManager.AddToRoleAsync(user, role.Name);

            var addUserToRoleViewModel = new UserToRoleViewModel
            {
                RoleId = role.Id,
                Users = new List<ApplicationUser>()
            };

            foreach (var us in _userManager.Users)
            {
                if (!await _userManager.IsInRoleAsync(us, role.Name))
                {
                    addUserToRoleViewModel.Users.Add(us);
                }
            }

            if (result.Succeeded)
            {                
                return View(addUserToRoleViewModel);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(addUserToRoleViewModel);
        }

        public async Task<IActionResult> DeleteUserFromRole(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
                return RedirectToAction("RoleManagement", _roleManager.Roles);

            var addUserToRoleViewModel = new UserToRoleViewModel
            {
                RoleId = role.Id,
                Users = new List<ApplicationUser>()
            };

            foreach (var user in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    addUserToRoleViewModel.Users.Add(user);
                }
            }

            return View(addUserToRoleViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUserFromRole(UserToRoleViewModel userRoleViewModel)
        {
            if (userRoleViewModel.RoleId == null)
            {
                ModelState.AddModelError("", "Role not specified.");
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }

            var role = await _roleManager.FindByIdAsync(userRoleViewModel.RoleId);

            if (role == null)
            {
                ModelState.AddModelError("", "Users or role not specified.");
                return RedirectToAction("RoleManagement", _roleManager.Roles);
            }

            else if (userRoleViewModel.UserId == null)
            {
                ModelState.AddModelError("", "User not specified.");
                return RedirectToAction("RemoveUserFromRole", userRoleViewModel.RoleId);
            }

            var user = await _userManager.FindByIdAsync(userRoleViewModel.UserId);

            if (user == null)
            {
                ModelState.AddModelError("", "User not specified.");
                return RedirectToAction("RemoveUserFromRole", userRoleViewModel.RoleId);
            }

            var result = await _userManager.RemoveFromRoleAsync(user, role.Name);

            var deleteUserFromRole = new UserToRoleViewModel
            {
                RoleId = role.Id,
                Users = new List<ApplicationUser>()
            };

            foreach (var us in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(us, role.Name))
                {
                    deleteUserFromRole.Users.Add(us);
                }
            }

            if (result.Succeeded)
            {
                return View(deleteUserFromRole);
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(deleteUserFromRole);
        }

        public IActionResult OrderManagement()
        {
            var orders = _orderRepository.GetAvailableOrders();

            return View(orders);
        }

        //public IActionResult AcceptOrder(int id)
        //{
        //    var order = _orderRepository.GetOrderById(id);
        //    if (order != null)
        //    {
        //        order.OrderState = OrderState.Accepted;
        //        _orderRepository.Update(order);
        //    }

        //    return RedirectToAction("OrderManagement");
        //}

        public async Task<IActionResult> RejectOrder(int id)
        {
            var order = _orderRepository.GetOrderById(id);
            if (order != null)
            {
                order.OrderState = OrderState.Rejected;
                _orderRepository.Update(order);

                var client = await _userManager.FindByEmailAsync(order.Email);

                var role = await _roleManager.FindByNameAsync("Administrator");
                var admins = new List<ApplicationUser>();

                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        admins.Add(user);
                    }
                }

                var emailSender = EmailSender.Instance;
                emailSender.NotifyAdminsRejectedOrder(order, admins);
                emailSender.NotifyClientRejectedOrder(order, client);

                if (client != null)
                {
                    _ordersArchiveRepository.AddOrderToArchiveByUserId(client.Id, order);
                }
            }
                       
            return RedirectToAction("OrderManagement");
        }


        public async Task<IActionResult> CompleteOrder(int id)
        {
            var order = _orderRepository.GetOrderById(id);
            if (order != null)
            {
                order.OrderState = OrderState.Completed;
                _orderRepository.Update(order);

                var client = await _userManager.FindByEmailAsync(order.Email);

                var role = await _roleManager.FindByNameAsync("Administrator");
                var admins = new List<ApplicationUser>();

                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        admins.Add(user);
                    }
                }

                var emailSender = EmailSender.Instance;
                emailSender.NotifyAdminsCompletedOrder(order, admins);
                emailSender.NotifyClientCompletedOrder(order, client);


                if (client != null)
                {
                    _ordersArchiveRepository.AddOrderToArchiveByUserId(client.Id, order);
                }
            }
            return RedirectToAction("OrderManagement");
        }

        public IActionResult RemoveOrder(int id)
        {
            var order = _orderRepository.GetOrderById(id);
            if (order != null)
            {
                _orderRepository.RemoveOrderById(id);
            }
            return RedirectToAction("Archive","Order");
        }

        public IActionResult CakeManagement()
        {
            var cakes = _cakeRepository.GetAllCakes();

            return View(cakes);
        }

        [HttpGet]
        public IActionResult EditCake(int id)
        {            
            var cake = _cakeRepository.GetCakeById(id);
            return View(cake);
        }

        [HttpPost]
        public IActionResult EditCake(Cake cake)
        {
            var cakeToUpdate = _cakeRepository.GetCakeById(cake.Id);
            if (!ModelState.IsValid || cakeToUpdate == null)
            {
                ModelState.AddModelError("", "Invalid cake information.");
                return View(cakeToUpdate);
            }

            cakeToUpdate.CakeStatus = cake.CakeStatus;
            cakeToUpdate.Description = cake.Description;
            cakeToUpdate.Name = cake.Name;
            cakeToUpdate.Price = cake.Price;


            _cakeRepository.Update(cakeToUpdate);

            return View("EditCake", _cakeRepository.GetCakeById(cakeToUpdate.Id));
        }

        public IActionResult RemoveCake(int id)
        {
            var cake = _cakeRepository.GetCakeById(id);
            if (cake != null)
            {
                var result = _cakeRepository.Remove(cake);
                if (result)
                    return View("CakeManagement", _cakeRepository.GetAllCakes());
                ModelState.AddModelError("", "Something went wrong while deleting this cake.");
            }
            else
            {
                ModelState.AddModelError("", "This cake can't be found.");
            }
            
            return View("CakeManagement", _cakeRepository.GetAllCakes());
        }

        [HttpGet]
        public IActionResult AddCake()
        {
            Cake cake = new Cake();
            return View(cake);
        }

        [HttpPost]
        public IActionResult AddCake(Cake cake)
        {
            cake.Id = 0;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Invalid cake information.");
                return View(cake);
            }

            var cakeAdded = _cakeRepository.Create(cake);
            if (cakeAdded == null)
            {
                ModelState.AddModelError("", "This cake cannot be added.");
                return View(cake);
            }              

            return RedirectToAction("UpdateImage", new {id = cakeAdded.Id, header = "add"});
        }

        public IActionResult UpdateImage(int id, string header)
        {
            if (header == null)
            {
                ViewData["HeaderInfo"] = "Choose a new image and click upload:";
            }
            else if (header.Equals("add"))
            {
                ViewData["HeaderInfo"] = "Your cake has been added, last step choose an image and click upload:";
            }
            

            var cake = _cakeRepository.GetCakeById(id);
            var model = new UpdateImageViewModel()
            {
                SmallImage = cake.SmallImage,
                CakeId = cake.Id
            };        
            return View(model);
        }
    }
}
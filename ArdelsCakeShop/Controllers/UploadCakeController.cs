﻿using System;
using System.IO;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArdelsCakeShop.Controllers
{
    public class UploadCakeController : Controller
    {


        private readonly ICakeRepository _cakeRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadCakeController(ICakeRepository cakeRepository, IHostingEnvironment hostingEnvironment)
        {
            _cakeRepository = cakeRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Upload(IFormFile formFile, int cakeId)
        {
            if (formFile == null || cakeId == 0)
            {
                return RedirectToAction("CakeManagement", "Admin",_cakeRepository.GetAllCakes());
            }

            var cake = _cakeRepository.GetCakeById(cakeId);
            // process uploaded files
            var dotSplitted = formFile.FileName.Split('.');
            var ext = dotSplitted[dotSplitted.Length - 1];

            if (ext.Equals("jpg") || ext.Equals("png"))
            {
                string webRootPath = _hostingEnvironment.WebRootPath.Replace("\\", "/");
                var imagesPath = Startup.HiddenKeys["ImagesPath"];
                var myUniqueFileName = string.Format(@"{0}.{1}", Guid.NewGuid(), ext);

                var relativePath = imagesPath + myUniqueFileName;
                var filePath = webRootPath + relativePath;

                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }

                    if (cake.SmallImage != null)
                    {
                        try
                        {
                            var oldFile = webRootPath + cake.SmallImage;
                            System.IO.File.Delete(oldFile);
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Something went wrong uploading the new cake.");
                            return RedirectToAction("EditCake", "Admin", cake);
                        }
                    }
                    
                    cake.SmallImage = relativePath;
                    _cakeRepository.Update(cake);

                    return RedirectToAction("EditCake", "Admin", cake);

                }
            }
            else
            {
                ModelState.AddModelError("", "Picture extension not available, please use jpg or png.");
            }
            return RedirectToAction("EditCake", "Admin", cake);
        }
    }
}
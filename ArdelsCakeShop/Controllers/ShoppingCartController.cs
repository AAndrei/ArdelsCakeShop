﻿using System.Linq;
using ArdelsCakeShop.Models.Repository;
using ArdelsCakeShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ArdelsCakeShop.Controllers
{
    public class ShoppingCartController : Controller
    {
        private const int MaxAmount = 50;
        private readonly ICakeRepository _cakeRepository;
        private readonly ShoppingCart _shoppingCart;

        public ShoppingCartController(ICakeRepository cakeRepository, ShoppingCart shoppingCart)
        {
            _cakeRepository = cakeRepository;
            _shoppingCart = shoppingCart;
        }

        public IActionResult Index()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };

            return View(shoppingCartViewModel);
        }

        public IActionResult AddToShoppingCart(int id, int amount)
        {
            var selectedCake = _cakeRepository.GetCakeById(id);

            if (selectedCake != null)
            {
                if (amount == 0 || amount < 0 || amount > MaxAmount)
                {
                    _shoppingCart.AddToCart(selectedCake, 1);
                }
                else
                {
                    _shoppingCart.AddToCart(selectedCake, amount);
                }
            }

            return Json(Ok("Added to cart!"));
        }

        public IActionResult RemoveOne(int id)
        {
            var selectedCake = _cakeRepository.GetCakeById(id);

            if (selectedCake != null)
            {
                var amount = _shoppingCart.RemoveFromCart(selectedCake, 1);                
            }

            return RedirectToAction("Index");
        }

        public IActionResult RemoveAll(int id)
        {
            var selectedCake = _cakeRepository.GetCakeById(id);

            if (selectedCake != null)
            {
                var amount = _shoppingCart.RemoveFromCart(selectedCake, MaxAmount);                
            }

            return RedirectToAction("Index");
        }


        public IActionResult ShoppingCartSummaryData()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            return Json(_shoppingCart.ShoppingCartItems.Sum(s => s.Amount));
        }
    }
}
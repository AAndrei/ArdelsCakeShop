﻿using System.Collections.Generic;
using System.Linq;
using ArdelsCakeShop.Models.Cakes;
using ArdelsCakeShop.Models.Repository;
using ArdelsCakeShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ArdelsCakeShop.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CakeDataController : Controller
    {
        private readonly ICakeRepository _cakeRepository;
        private const int PAGE_SIZE = 10;        

        public CakeDataController(ICakeRepository cakeRepository)
        {
            _cakeRepository = cakeRepository;
        }

        [HttpGet]
        public IEnumerable<CakeViewModel> LoadMoreCakes(int currentPage)
        {
            var total = _cakeRepository.GetAllCakes().Count();
            var skip = PAGE_SIZE * currentPage;
            if (skip > total)
                return null;

            IEnumerable<Cake> dbCakes = null;
            dbCakes = _cakeRepository.GetAllCakes().OrderBy(c => c.Id).Skip(skip).Take(PAGE_SIZE);

            List<CakeViewModel> cakes = new List<CakeViewModel>();
            foreach (var cake in dbCakes)
            {
                cakes.Add(MapDbCakeToCakeViewMolde(cake));
            }
            return cakes;
        }

        private static CakeViewModel MapDbCakeToCakeViewMolde(Cake cake)
        {
            return new CakeViewModel
            {
                CakeId = cake.Id,
                Name = cake.Name, 
                Price = cake.Price,
                ShortDescription = cake.Description,
                SmallImage = cake.SmallImage
            };
        }

        [HttpGet("{id}")]
        public CakeViewModel LoadCakeById(int id)
        {
            return new CakeViewModel();
        } 
    }
}
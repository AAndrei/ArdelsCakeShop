﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ArdelsCakeShop.ViewModels;
using ArdelsCakeShop.Controllers.Utilities;
using ArdelsCakeShop.Models.Repository;

namespace ArdelsCakeShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICakeRepository _cakeRepository;        

        public HomeController(ICakeRepository cakeRepository)
        {
            _cakeRepository = cakeRepository;
        }

        public IActionResult Index()
        {
            var cakes = _cakeRepository.GetAllCakes();
            return View(cakes);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            
            return View();
        }
            
        [HttpPost]
        public IActionResult Contact(ContactUsViewModel contactUsViewModel)
        {
                if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Invalid inputs!");
                return View();
            }

            var emailSender = EmailSender.Instance;

            try
            {
                emailSender.Receive(contactUsViewModel.Email, "Contact us mail", contactUsViewModel.Comment);
            }catch(Exception){
                ModelState.AddModelError("", "Email address isn't available at the moment!");
                return View();
            }
            ViewBag.Success = "Your message was succesfully sent!";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArdelsCakeShop.Controllers.Utilities;
using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.Models.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.Orders;
using ArdelsCakeShop.ViewModels;
using Microsoft.AspNetCore.Authorization;


namespace ArdelsCakeShop.Controllers
{
    public class OrderController: Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrdersArchiveRepository _ordersArchiveRepository;
        private readonly ShoppingCart _shoppingCart;
        private readonly EmailSender _emailSender;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public OrderController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            IOrderRepository orderRepository, ShoppingCart shoppingCart, IOrdersArchiveRepository ordersArchiveRepository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _orderRepository = orderRepository;
            _ordersArchiveRepository = ordersArchiveRepository;
            _shoppingCart = shoppingCart;
        }
        [Authorize]
        public async Task<IActionResult> Archive()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var model = new OrdersArchiveViewModel();

            if (User.IsInRole("Administrator"))
            {
                var entireArchive = _ordersArchiveRepository.GetArchives();

                if (entireArchive != null)
                {
                    var orders = new List<Order>();

                    var ordersArchives = entireArchive.ToList();
                    foreach (var oa in ordersArchives)
                    {
                        orders.AddRange(oa.Orders);
                    }

                    model.Orders = orders;
                }
                else
                {
                    model.Orders = new List<Order>();
                }

                return View(model);
            }
            var archive = _ordersArchiveRepository.GetArchiveByUserId(user.Id);

            model.Orders = archive != null ? archive.Orders : new List<Order>();

            return View(model);
        }

        public async Task<IActionResult> Checkout()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user != null)
            {
                var model = new Order
                {
                    AddressLine = user.Address,
                    PhoneNumber = user.PhoneNumber,
                    Email = user.Email
                };
                return View(model);
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Checkout(Order order)
        {                        
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            if (_shoppingCart.ShoppingCartItems.Count == 0)
            {
                ModelState.AddModelError("", "Your cart is empty, add some cakes.");
            }

            if (ModelState.IsValid)
            {
                order.ConfirmationToken = Guid.NewGuid().ToString();
                var link = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}" + "/Order/ConfirmOrder?token=" + order.ConfirmationToken;
                
                // place the order
                _orderRepository.CreateOrder(order);
                _shoppingCart.ClearCart();

                //notify admins that a new order has been placed
                var role = await _roleManager.FindByNameAsync("Administrator");
                var admins = new List<ApplicationUser>();
                
                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        admins.Add(user);
                    }
                }

                var client = new ApplicationUser
                {
                    Email = order.Email,
                    Address = order.AddressLine,                    
                    PhoneNumber = order.PhoneNumber
                };


                var emailSender = EmailSender.Instance;
                emailSender.NotifyAdminsNewOrder(order, admins);
                emailSender.NotifyClientNewOrder(order, client, link);                

                return RedirectToAction("CheckoutComplete");
            }

            return View(order);
        }              

        public IActionResult CheckoutComplete()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmOrder(string token)
        {
            var order = _orderRepository.GetOrders().AsQueryable().FirstOrDefault(o => o.ConfirmationToken == token);

            var model = new ConfirmOrderViewModel();
            
            if (order != null)
            {
                order.OrderState = OrderState.Accepted;
                order.ConfirmationToken = null;
                _orderRepository.Update(order);

                var role = await _roleManager.FindByNameAsync("Administrator");
                var admins = new List<ApplicationUser>();

                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        admins.Add(user);
                    }
                }

                var emailSender = EmailSender.Instance;

                model.Message = "You're order has been accepted!";
                emailSender.NotifyAdminsConfirmedOrder(order, admins);


                // add order to archive
                var client = await _userManager.FindByEmailAsync(order.Email);

                if (client != null)
                {
                    _ordersArchiveRepository.AddOrderToArchiveByUserId(client.Id, order);
                }

                return View(model);
            }

            model.Message = "This link is not valid any longer!";            

            return View(model);           
        }

    }
}

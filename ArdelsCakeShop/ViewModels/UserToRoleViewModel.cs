﻿using ArdelsCakeShop.Models.IdentityModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class UserToRoleViewModel
    {
        [Required]
        [StringLength(100)]
        public string RoleId { get; set; }
        [Required]
        [StringLength(100)]
        public string UserId { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}

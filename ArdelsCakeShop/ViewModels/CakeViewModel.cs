﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class CakeViewModel
    {
        public int CakeId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortDescription { get; set; }
        public decimal Price { get; set; }
        [StringLength(100)]
        public string SmallImage { get; set; }
    }
}

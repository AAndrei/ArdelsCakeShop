﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class EditProfileViewModel
    {
        public string Username { get; set; }                
        public string Email { get; set; }
        [Required(ErrorMessage = "Mobile no. is required")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.(like xxx-xxx-xxxx)")]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string Address { get; set; }        
    }
}

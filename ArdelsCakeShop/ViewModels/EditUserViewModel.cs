﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class EditUserViewModel
    {
        [Required]
        [StringLength(100)]
        public string Id { get; set; }
        [Required]
        [StringLength(100)]
        public string UserName { get; set; }
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }
    }
}

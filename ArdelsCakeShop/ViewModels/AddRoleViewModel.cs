﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class AddRoleViewModel
    {
        [Required]
        [Display(Name = "Role name")]
        [StringLength(100)]
        public string RoleName { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class EditRoleViewModel
    {
        [Required]
        [StringLength(100)]
        public string Id { get; set; }
        [Required]
        [StringLength(100)]
        public string RoleName { get; set; }
        public List<string> Users { get; set; }
    }
}
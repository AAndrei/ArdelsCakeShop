﻿using Microsoft.AspNetCore.Http;

namespace ArdelsCakeShop.ViewModels
{
    public class UpdateImageViewModel
    {
        public string SmallImage { get; set; }
        public IFormFile FormFile { get; set; }
        public int CakeId { get; set; }
    }
}

﻿using ArdelsCakeShop.Models.Repository;

namespace ArdelsCakeShop.ViewModels
{
    public class ShoppingCartViewModel
    {
        public ShoppingCart ShoppingCart { get; set; }
        public decimal ShoppingCartTotal { get; set; }
    }
}

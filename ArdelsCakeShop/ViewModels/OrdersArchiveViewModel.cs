﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.Orders;

namespace ArdelsCakeShop.ViewModels
{
    public class OrdersArchiveViewModel
    {
        public List<Order> Orders { get; set; }
    }
}

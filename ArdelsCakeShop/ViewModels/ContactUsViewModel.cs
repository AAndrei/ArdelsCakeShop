﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.ViewModels
{
    public class ContactUsViewModel
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [StringLength(2000,MinimumLength = 10, ErrorMessage ="Your message must be between 10 to 2000 letters")]
        public string Comment { get; set; }
    }
}

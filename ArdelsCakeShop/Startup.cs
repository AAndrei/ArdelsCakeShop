﻿using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.Models.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace ArdelsCakeShop
{
    public class Startup
    {
        private string _appKeys = null;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }      

        public IConfiguration Configuration { get; }
        public static IDictionary<string, string> HiddenKeys;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, AppClaimsPrincipalFactory>();

            services.AddTransient<ICakeRepository, CakeRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrdersArchiveRepository, OrdersArchiveRepository>();

            services.AddMemoryCache();
            services.AddSession();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ShoppingCart>(sp => ShoppingCart.GetCart(sp));

            services.AddMvc();
            services.AddSingleton(Configuration);            

            services.AddAuthentication()
                .AddFacebook(options =>
                {

                    options.AppId = Configuration["Facebook:AppId"];
                    options.AppSecret = Configuration["Facebook:AppSecret"];
                })
                .AddCookie();

            HiddenKeys = new Dictionary<string, string>
            {
                {"Username", Configuration["GmailCredentials:Username"]},
                {"Password", Configuration["GmailCredentials:Password"]},
                {"ImagesPath", Configuration["ImagesPath"]}
            };
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {         
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            
            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //   name: "addToShoppingCart",
                //   template: "ShoppingCart/{action}/{id?}",
                //   defaults: new { Controller = "ShoppingCart", action = "AddToShoppingCart" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
               
            });
        }
    }
}

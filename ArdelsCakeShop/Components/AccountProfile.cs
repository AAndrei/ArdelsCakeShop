﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.IdentityModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ArdelsCakeShop.Components
{
    public class AccountProfile : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountProfile(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user =await GetCurrentUserAsync();         

            ViewBag.User = user.UserName;            

            var menuItems = new List<AccountProfileItem> {
                new AccountProfileItem()
                {
                    DisplayValue = "Edit profile",
                    ActionValue = "Profile"                    
                },
                new AccountProfileItem()
                {
                    DisplayValue = "Logout",
                    ActionValue = "Logout"

                }};

            return View(menuItems);
            }
        }

        public class AccountProfileItem
    {
            public string DisplayValue { get; set; }
            public string ActionValue { get; set; }
        }    

}

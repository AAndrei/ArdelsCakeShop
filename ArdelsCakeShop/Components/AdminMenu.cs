﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ArdelsCakeShop.Components
{
    public class AdminMenu : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var menuItems = new List<AdminMenuItem> {
                new AdminMenuItem()
                {
                    DisplayValue = "User management",
                    ActionValue = "UserManagement"

                },
                new AdminMenuItem()
                {
                    DisplayValue = "Role management",
                    ActionValue = "RoleManagement"
                },
                new AdminMenuItem()
                {
                    DisplayValue = "Cake management",
                    ActionValue = "CakeManagement"
                },
                new AdminMenuItem()
                {
                    DisplayValue = "Order management",
                    ActionValue = "OrderManagement"

                }};

            return View(menuItems);
        }
    }

    public class AdminMenuItem
    {
        public string DisplayValue { get; set; }
        public string ActionValue { get; set; }
    }
}

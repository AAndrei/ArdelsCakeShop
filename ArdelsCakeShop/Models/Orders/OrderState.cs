﻿
namespace ArdelsCakeShop.Models.Orders
{    
        public enum OrderState
        {
            Pending,
            Accepted,
            Completed,            
            Rejected
        };    
}

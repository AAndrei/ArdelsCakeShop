﻿using ArdelsCakeShop.Models.IdentityModel;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace ArdelsCakeShop.Models.Orders
{
    public class OrdersArchive
    {
        [BindNever]
        public int OrdersArchiveId { get; set; }
        public string UserId { get; set; }
        public List<Order> Orders { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}

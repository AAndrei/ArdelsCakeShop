﻿using System.Collections.Generic;
using System.Linq;
using ArdelsCakeShop.Models.Cakes;
using Microsoft.EntityFrameworkCore;

namespace ArdelsCakeShop.Models.Repository
{
    public class CakeRepository : ICakeRepository
    {
        private readonly AppDbContext _appDbContext;

        public CakeRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Cake> GetAllCakes()
        {
            return _appDbContext.Cakes;            
        }

        public Cake GetCakeById(int cakeId)
        {
            return _appDbContext.Cakes.FirstOrDefault(c => c.Id == cakeId);
        }

        public bool Remove(Cake cake)
        {
            var cakeRemoved = _appDbContext.Cakes.Remove(cake);

            if (cakeRemoved.State == EntityState.Deleted)
            {
                _appDbContext.SaveChanges();

                return true;
            }

            return false;
        }

        public void Update(Cake cake)
        {
            _appDbContext.Cakes.Update(cake);            
            _appDbContext.SaveChanges();           
        }

        public Cake Create(Cake cake)
        {
            var cakeAdded = _appDbContext.Cakes.Add(cake);

            if (cakeAdded.State == EntityState.Added)
            {
                _appDbContext.SaveChanges();

                return cakeAdded.Entity;
            }

            return null;
        }
    }
}

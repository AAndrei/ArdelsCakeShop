﻿
using System.Collections.Generic;
using ArdelsCakeShop.Models.Orders;

namespace ArdelsCakeShop.Models.Repository
{
    public interface IOrdersArchiveRepository
    {
        IEnumerable<OrdersArchive> GetArchives();
        OrdersArchive GetArchiveByUserId(string userId);
        OrdersArchive AddOrderToArchiveByUserId(string userId, Order order);
        void RemoveOrderFromArchiveByUserId(string userId, Order order);
        void ClearArchiveByUserId(string userId);
    }
}

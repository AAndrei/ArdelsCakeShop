﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ArdelsCakeShop.Models.Orders;

namespace ArdelsCakeShop.Models.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly ShoppingCart _shoppingCart;

        public OrderRepository(AppDbContext appDbContext, ShoppingCart shoppingCart)
        {
            _appDbContext = appDbContext;
            _shoppingCart = shoppingCart;
        }

        public void CreateOrder(Order order)
        {
            order.OrderPlaced = DateTime.Now;
            order.OrderTotal = _shoppingCart.GetShoppingCartTotal();

            _appDbContext.Orders.Add(order);

            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            foreach (var shoppingCartItem in shoppingCartItems)
            {
                var orderDetail = new OrderDetail()
                {
                    Amount = shoppingCartItem.Amount,
                    CakeId = shoppingCartItem.Cake.Id,
                    OrderId = order.OrderId,
                    Price = shoppingCartItem.Cake.Price
                };

                _appDbContext.OrderDetails.Add(orderDetail);
            }

            _appDbContext.SaveChanges();
        }

        public IEnumerable<Order> GetOrders()
        {
            return _appDbContext.Orders.Include(o => o.OrderLines).ThenInclude(o => o.Cake);            
        }

        public IEnumerable<Order> GetAvailableOrders()
        {
            return _appDbContext.Orders.Where(o => o.OrderState == OrderState.Pending || o.OrderState == OrderState.Accepted)
                .Include(o => o.OrderLines).ThenInclude(o => o.Cake);
        }

        public void RemoveOrderById(int id)
        {
            var order = _appDbContext.Orders.Find(id);
            _appDbContext.Remove(order);
            _appDbContext.SaveChanges();
        }

        public Order GetOrderById(int id)
        {
            var order = _appDbContext.Orders
                .Include(o => o.OrderLines).ThenInclude(o => o.Cake)
                .FirstOrDefault(o => o.OrderId == id); 
            return order;
        }

        public void Update(Order order)
        {        
            _appDbContext.Orders.Update(order);
            _appDbContext.SaveChanges();
        
        }
    }
}

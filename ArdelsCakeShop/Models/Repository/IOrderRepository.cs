﻿using System.Collections.Generic;
using ArdelsCakeShop.Models.Orders;

namespace ArdelsCakeShop.Models.Repository
{
    public interface IOrderRepository
    {
        void CreateOrder(Order order);
        IEnumerable<Order> GetOrders();
        void RemoveOrderById(int id);
        Order GetOrderById(int id);
        void Update(Order order);
        IEnumerable<Order> GetAvailableOrders();
    }
}

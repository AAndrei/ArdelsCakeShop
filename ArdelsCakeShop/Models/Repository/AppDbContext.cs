﻿using ArdelsCakeShop.Models.Cakes;
using ArdelsCakeShop.Models.IdentityModel;
using ArdelsCakeShop.Models.Orders;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ArdelsCakeShop.Models.Repository
{
    public class AppDbContext: IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> dbContextOptions)
     : base(dbContextOptions)
        {

        }        

        public DbSet<Cake> Cakes { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<OrdersArchive> OrdersArchives{ get; set; }
    }
}

﻿using System.Collections.Generic;
using ArdelsCakeShop.Models.Cakes;

namespace ArdelsCakeShop.Models.Repository
{
    public interface ICakeRepository
    {
        IEnumerable<Cake> GetAllCakes();
        Cake GetCakeById(int cakeId);
        bool Remove(Cake cake);
        void Update(Cake cake);
        Cake Create(Cake cake);
    }
}

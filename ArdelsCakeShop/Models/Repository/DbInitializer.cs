﻿using System.Linq;
using ArdelsCakeShop.Models.Cakes;

namespace ArdelsCakeShop.Models.Repository
{
    public static class DbInitializer
    {
        private const string ImagesPath = "/images/";

        public static void Seed(AppDbContext appDbContext)
        {
            if (!appDbContext.Cakes.Any())
            {
                appDbContext.AddRange
                    (
                    new Cake { Name = "Angel Cake", Description = "Sponge cake, cream", Price = 10.99M, SmallImage= ImagesPath + "angel_cake.jpg" },
                    new Cake { Name = "Apple Tart", Description = "Apple, caramel icing", Price = 8.99M, SmallImage = ImagesPath + "apple_tart.jpg" },
                    new Cake { Name = "Aranygaluska", Description = "A cake with yeasty dough and vanilla custard", Price = 11.99M, SmallImage = ImagesPath + "aranygaluska.jpg" },
                    new Cake { Name = "Babka", Description = "Easter cake with icing", Price = 10.99M, SmallImage = ImagesPath + "babka_wielkanocna.jpg" },
                    new Cake { Name = "Banana Cake", Description = "Prepared using banana as a primary ingredient", Price = 10.99M, SmallImage = ImagesPath + "banana_cake.jpg" },
                    new Cake { Name = "Bebinca", Description = "Flour, sugar, ghee (clarified butter), coconut milk, egg yolk", Price = 10.99M, SmallImage = ImagesPath + "bebinca_com_gelado.jpg" },
                    new Cake { Name = "Beer Cake", Description = "Cake prepared with beer as a main ingredient. Pictured is a chocolate bundt cake infused with stout beer.", Price = 10.99M, SmallImage = ImagesPath + "chocolate_stout_cake.jpg" },
                    new Cake { Name = "Boston Cream Pie", Description = "Egg custard, chocolate", Price = 10.99M, SmallImage = ImagesPath + "boston_cream_pie.jpg" },
                    new Cake { Name = "Batik Cake", Description = "A non-baked cake dessert made by mixing broken Marie biscuits, combined with a chocolate sauce or runny custard.", Price = 10.99M, SmallImage = ImagesPath + "malaysian_batik_cake.jpg" },
                    new Cake { Name = "Avocado Cake", Description = "Prepared using avocado as a primary ingredient", Price = 10.99M, SmallImage = ImagesPath + "mocha_almond_fudge_avocado_cake.jpg" }
                    );
                appDbContext.SaveChanges();
            }
        }
    }
}

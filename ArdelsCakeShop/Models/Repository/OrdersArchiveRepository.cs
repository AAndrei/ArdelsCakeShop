﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArdelsCakeShop.Models.Orders;
using Microsoft.EntityFrameworkCore;

namespace ArdelsCakeShop.Models.Repository
{
    public class OrdersArchiveRepository: IOrdersArchiveRepository
    {
        private readonly AppDbContext _appDbContext;

        public OrdersArchiveRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<OrdersArchive> GetArchives()
        {
            return _appDbContext.OrdersArchives
                .Include(o => o.Orders).ThenInclude(or => or.OrderLines).ThenInclude(or => or.Cake);
        }

        public OrdersArchive GetArchiveByUserId(string userId)
        {
            return _appDbContext.OrdersArchives
                .Include(o => o.Orders).ThenInclude(or => or.OrderLines).ThenInclude(or => or.Cake)
                .FirstOrDefault(a => a.UserId.Equals(userId));
        }

        public OrdersArchive AddOrderToArchiveByUserId(string userId, Order order)
        {
            var archive = GetArchiveByUserId(userId);

            if (archive != null)
            {
                if (archive.Orders.Contains(order))
                {
                    return archive;
                }

                archive.Orders.Add(order);

                _appDbContext.OrdersArchives.Update(archive);
                _appDbContext.SaveChanges();

                return archive;
            }
            
            var newArchive = new OrdersArchive
            {
                UserId = userId,
                Orders = new List<Order> { order },
            };
            
            var addedArchive = _appDbContext.OrdersArchives.Add(newArchive);
            _appDbContext.SaveChanges();

            return addedArchive.Entity;
        }

        public void RemoveOrderFromArchiveByUserId(string userId, Order order)
        {
            throw new NotImplementedException();
        }

        public void ClearArchiveByUserId(string userId)
        {
            throw new NotImplementedException();
        }
    }
}

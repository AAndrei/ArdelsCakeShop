﻿namespace ArdelsCakeShop.Models.Cakes
{
    public enum CakeStatus
    {
        Available,
        Unavailable
    }
}
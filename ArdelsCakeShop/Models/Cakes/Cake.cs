﻿using System.ComponentModel.DataAnnotations;

namespace ArdelsCakeShop.Models.Cakes
{
    public class Cake
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(300)]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [StringLength(300)]
        public string SmallImage { get; set; } = "/images/default.jpg";
        [Display(Name = "Status")]
        public CakeStatus CakeStatus { get; set; } = CakeStatus.Available;
    }
}

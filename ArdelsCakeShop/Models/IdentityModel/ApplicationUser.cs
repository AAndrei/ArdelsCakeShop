﻿using Microsoft.AspNetCore.Identity;

namespace ArdelsCakeShop.Models.IdentityModel
{
    public class ApplicationUser: IdentityUser
    {
        public string Address { get; set; }
    }
}

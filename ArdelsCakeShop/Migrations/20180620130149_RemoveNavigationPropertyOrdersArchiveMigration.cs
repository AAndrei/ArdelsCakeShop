﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ArdelsCakeShop.Migrations
{
    public partial class RemoveNavigationPropertyOrdersArchiveMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders");

            migrationBuilder.AlterColumn<int>(
                name: "OrdersArchiveId",
                table: "Orders",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders",
                column: "OrdersArchiveId",
                principalTable: "OrdersArchives",
                principalColumn: "OrdersArchiveId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders");

            migrationBuilder.AlterColumn<int>(
                name: "OrdersArchiveId",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders",
                column: "OrdersArchiveId",
                principalTable: "OrdersArchives",
                principalColumn: "OrdersArchiveId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

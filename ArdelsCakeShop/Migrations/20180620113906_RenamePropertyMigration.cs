﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ArdelsCakeShop.Migrations
{
    public partial class RenamePropertyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "OrdersArchives",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersArchives_UserId",
                table: "OrdersArchives",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersArchives_AspNetUsers_UserId",
                table: "OrdersArchives",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersArchives_AspNetUsers_UserId",
                table: "OrdersArchives");

            migrationBuilder.DropIndex(
                name: "IX_OrdersArchives_UserId",
                table: "OrdersArchives");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "OrdersArchives",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

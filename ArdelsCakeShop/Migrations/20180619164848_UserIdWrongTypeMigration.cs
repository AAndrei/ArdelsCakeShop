﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ArdelsCakeShop.Migrations
{
    public partial class UserIdWrongTypeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrdersArchiveId",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OrdersArchives",
                columns: table => new
                {
                    OrdersArchiveId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdersArchives", x => x.OrdersArchiveId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrdersArchiveId",
                table: "Orders",
                column: "OrdersArchiveId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders",
                column: "OrdersArchiveId",
                principalTable: "OrdersArchives",
                principalColumn: "OrdersArchiveId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrdersArchives_OrdersArchiveId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "OrdersArchives");

            migrationBuilder.DropIndex(
                name: "IX_Orders_OrdersArchiveId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrdersArchiveId",
                table: "Orders");
        }
    }
}
